<?php
$ef_options = EF_Event_Options::get_theme_options();
?>

<footer>
    <div class="container">
        <div class="row row-sm">
            <?php dynamic_sidebar('footer'); ?>
        </div>
    </div>
    <div class="credits">
        <?php 
        if ( isset( $ef_options['ef_footer_content'] ) ) {
        	echo stripslashes( $ef_options['ef_footer_content'] ); 
        }
        ?>
        <div class="footer-tyler-event"><span class="footer-icons">Presented by<br><a href="http://www.canadiancloudcouncil.ca" target="_blank"><img class="footer-icons-ccc" src="http://interzone.factori.com/cms/wp-content/uploads/2014/05/ccc_footer_graphic.png"></a><a href="http://www.factori.com" target="_blank"><img class="footer-icons-fo" src="http://interzone.factori.com/cms/wp-content/uploads/2014/05/fo_footer_graphic.png"></a></span><br>
        	© 2014 Factori Communications Ltd. All rights reserved.<br>
INTERZONE™, Factori™, SubCulture™ and Politik™ are trademarks of Factori Communications Ltd.<br>
Interzone 2015 is presented by the <a href="http://www.canadiancloudcouncil.ca" target="_blank">Canadian Cloud Council</a> and <a href="http://www.factori.com" target="_blank">Factori</a>.<br>
<span class="privacy-link" style ="font-size:10px"><a href="http://interzone.factori.com/privacy-policy/">Privacy Policy</a></span>
        </div>
    </div>
</footer>
<?php wp_footer(); ?>

<!-- SCROLL UP BTN -->
<a href="#" id="scroll-up"><?php _e('UP', 'tyler'); ?></a>

<!-- The Gallery as lightbox dialog, should be a child element of the document body -->
<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">
    <div class="slides"></div>
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
</div>

<!-- backdrop -->
<div id="backdrop"></div>
<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-51514445-1', 'factori.com');
  ga('send', 'pageview');

</script>
<!-- AdRoll -->
<script type="text/javascript">
adroll_adv_id = "HQGBVTDRQFEFHC4KX6GND2";
adroll_pix_id = "SHDXQITCVRGIJO676ZWUML";
(function () {
var oldonload = window.onload;
window.onload = function(){
   __adroll_loaded=true;
   var scr = document.createElement("script");
   var host = (("https:" == document.location.protocol) ? "https://s.adroll.com" : "http://a.adroll.com");
   scr.setAttribute('async', 'true');
   scr.type = "text/javascript";
   scr.src = host + "/j/roundtrip.js";
   ((document.getElementsByTagName('head') || [null])[0] ||
    document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
   if(oldonload){oldonload()}};
}());
</script>
</body>
</html>
