<?php
/*
 * Template Name: Sponsors
 *
 * @package WordPress
 * @subpackage Tyler
 */
?>
<?php get_header() ?>

<?php while (have_posts()) : the_post(); ?>
    <div class="heading">
        <div class="container">
            <h1><?php the_title(); ?></h1>
        </div>
    </div>
    <div class="container">
        <p>
            <?php the_content(); ?>
        </p>	    
		<!-- SPONSORS -->
		<div id="tile_sponsors" class="container widget">
			
			<h2>Sponsors Page Title</h2>
			<a href="#" class="btn btn-primary btn-header pull-right hidden-xs">
				Become a Sponsor
			</a>
			<h3>Sponsors Page Sub Title</h3>
			<br/><?php
			
			$categories	= get_terms( 'sponsor-tier' );
			
			foreach ( $categories as $category ) { ?>
				
				<h3 class="sponsor"><span><?php echo $category->name; ?></span></h3>
				<div class="sponsors sponsors-lg sponsors-<?php echo $category->name; ?>"><?php
					
					$sponsors_args	= array(
										'posts_per_page'	=> -1,
										'post_type'			=> 'sponsor',
										'tax_query'			=> array(
																	array(
																		'taxonomy'	=> 'sponsor-tier',
																		'field'		=> 'slug',
																		'terms'		=> array( $category->slug )
																	),
																)
									);
					
					$sponsors_chunks	= array_chunk( get_posts( $sponsors_args ), 3 );
					$sponsors_chunks	= apply_filters( 'multievent_filter_posts_ef_sponsors', $sponsors_chunks, $sponsors_args, $instance );
					
					if( !empty( $sponsors_chunks ) ) {
						
						foreach ( $sponsors_chunks as $chunk_key => $sponsors_chunk ) {?>
		
							<div class="item<?php if ($chunk_key == 0) echo ' active'; ?>"><?php
								if( !empty( $sponsors_chunk ) ) {
		
									foreach ( $sponsors_chunk as $sponsors ) {
									
										$link = get_post_meta( $sponsors->ID, 'sponsor_link', true );
		
										echo('<div class="sponsor">');
		
										if( $link ) {
										
											echo ("<a href='$link' title='" . $sponsors->post_title . "' target='_blank'>");
										}
		
										echo get_the_post_thumbnail( $sponsors->ID, 'full' );
		
										if( $link ) {
										
											echo ("</a>");
										}
		
										echo('</div>');
									}
								}?>
							</div><!-- .item --><?php 
						}
					}?>
				</div><!-- .sponsors .sponsors-lg --><?php
			}//end categories foreach loop?>
			
			<div class="text-center visible-xs">
				<a href="<?php echo stripslashes($sponsorsbuttonlink); ?>" class="btn btn-primary btn-header">
					<?php echo stripslashes($sponsorsbuttontext); ?>
				</a>
			</div>
		</div>	
    </div>
<?php endwhile; // end of the loop. ?>

<?php get_footer() ?>