<?php
// ******************* Add Libraries ****************** //
require_once('event-framework/lib/facebook/facebook.php');
require_once('event-framework/lib/twitter.php');
require_once('event-framework/lib/geocode.php');
require_once('event-framework/lib/recaptchalib.php');

include 'event-framework/event-framework.php';

add_filter( 'ef_theme_options_logo', 'tyler_set_theme_options_logo' );

function tyler_set_theme_options_logo( $url ) {
	return get_template_directory_uri() . '/images/schemes/basic/logo.png';
}

// Display the logo based on selected Color Scheme
function tyler_set_theme_logo() {
	// Get Theme Options
	$ef_options = EF_Event_Options::get_theme_options();
	
	$color_scheme = empty( $ef_options['ef_color_palette'] ) ? 'basic' : $ef_options['ef_color_palette'];
	
	if ( ! empty( $ef_options['ef_logo'] ) && $ef_options['ef_logo'] != 'http://' ) {
		$logo_url = $ef_options['ef_logo'];
	} else {
		$logo_url = get_stylesheet_directory_uri() . "/images/schemes/$color_scheme/logo.png";
	}
	
	return $logo_url;
}

function tyler_setup_social_networks() {
	global $twitter, $facebook;
	
	$facebookAppID = get_option('ef_facebook_rsvp_widget_appid');
	$facebookSecret = get_option('ef_facebook_rsvp_widget_secret');
	
	if (!empty($facebookAppID) && !empty($facebookSecret))
	    $facebook = new Facebook(array(
	                'appId' => $facebookAppID,
	                'secret' => $facebookSecret,
	            ));
	
	$twitterAccessToken = get_option('ef_twitter_widget_accesstoken');
	$twitterAccessTokenSecret = get_option('ef_twitter_widget_accesstokensecret');
	$twitterConsumerKey = get_option('ef_twitter_widget_consumerkey');
	$twitterConsumerSecret = get_option('ef_twitter_widget_consumersecret');
	
	if (!empty($twitterAccessToken) && !empty($twitterAccessTokenSecret) && !empty($twitterConsumerKey) && !empty($twitterConsumerSecret)) {
	    $twitter = new TwitterAPIExchange(array(
	                'oauth_access_token' => $twitterAccessToken,
	                'oauth_access_token_secret' => $twitterAccessTokenSecret,
	                'consumer_key' => $twitterConsumerKey,
	                'consumer_secret' => $twitterConsumerSecret
	            ));
	}
}

add_action( 'init', 'tyler_setup_social_networks' );

add_action('after_setup_theme', 'tyler_after_theme_setup');

function tyler_after_theme_setup() {

// ******************* Localizations ****************** //
    load_theme_textdomain( 'tyler', get_template_directory() . '/languages/' );

// ******************* Add Custom Menus ****************** //    
    add_theme_support('menus');

// ******************* Add Post Thumbnails ****************** //
    add_theme_support('post-thumbnails');
    add_image_size('tyler-speaker', 212, 212);
    add_image_size('tyler-media', 400, 245);
    add_image_size('tyler-blog-home', 355, 236);
    add_image_size('tyler-blog-list', 306, 188);
    add_image_size('tyler-blog-detail', 642, 428);
    add_image_size('tyler-content', 978, 389);

// ******************* Add Navigation Menu ****************** //    
    register_nav_menu('primary', __('Navigation Menu', 'tyler'));
}

// ******************* Scripts and Styles ****************** //
add_action('wp_enqueue_scripts', 'tyler_enqueue_scripts');

function tyler_enqueue_scripts() {
	// Get Theme Options
    $ef_options = EF_Event_Options::get_theme_options();

    // styles
    wp_enqueue_style('tyler-google-font', '//fonts.googleapis.com/css?family=Ubuntu:300,400,500,700');
    wp_enqueue_style('tyler-bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css');
    wp_enqueue_style('tyler-blueimp-gallery', get_template_directory_uri() . '/css/blueimp-gallery.min.css');
    wp_enqueue_style('tyler-jquery-scrollpane', get_template_directory_uri() . '/css/jquery.scrollpane.css');
    wp_enqueue_style('tyler-icons', get_template_directory_uri() . '/css/icon.css');
    wp_enqueue_style('tyler-layout', get_stylesheet_directory_uri() . '/css/layout.css');
    wp_enqueue_style('tyler-layout-mobile', get_stylesheet_directory_uri() . '/css/layout-mobile.css');
    
    // Color Schemes
    $color_scheme = empty( $ef_options['ef_color_palette'] ) ? 'basic' : $ef_options['ef_color_palette'];
    if ( isset( $color_scheme ) && $color_scheme != 'basic' ) {
    	wp_enqueue_style( $color_scheme . '-scheme', get_template_directory_uri() . '/css/schemes/' . $color_scheme . '/layout.css' );
    }
    
    if (get_page_template_slug() == 'twitter.php') {
    	wp_enqueue_script('jquery-tweet-machine', get_template_directory_uri() . '/js/tweetMachine.min.js', array('jquery'), false, true);
    	wp_enqueue_script('tyler-twitter', get_template_directory_uri() . '/js/twitter.js', array('jquery-tweet-machine'));
    }

    // scripts
    wp_enqueue_script('jquery');
    wp_enqueue_script('tyler-bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), false, true);
    wp_enqueue_script('tyler-blueimp-gallery', get_template_directory_uri() . '/js/blueimp-gallery.min.js', array('jquery'), false, true);
    wp_enqueue_script('tyler-jquery-mousewheel', get_template_directory_uri() . '/js/jquery.mousewheel.js', array('jquery'), false, true);
    wp_enqueue_script('tyler-jquery-jscrollpane', get_template_directory_uri() . '/js/jquery.jscrollpane.min.js', array('jquery'), false, true);
    wp_enqueue_script('tyler-script', get_template_directory_uri() . '/js/main.js', array('jquery'), false, true);

    // home
    if (is_home()) {
        wp_enqueue_script('tyler-home', get_template_directory_uri() . '/js/home.js', array('jquery'), false, true);
        wp_enqueue_script('jquery-tweet-machine', get_template_directory_uri() . '/js/tweetMachine.min.js', array('jquery'), false, true);
    }

    // single
    if (is_singular())
        wp_enqueue_script('comment-reply');

    // session
    if (is_singular(array('session'))) {
    	if ( ! empty( $ef_options['ef_add_this_pubid'] ) ) {
        	wp_enqueue_script('addthis', "//s7.addthis.com/js/300/addthis_widget.js#pubid={$ef_options['ef_add_this_pubid']}");
    	}
    }
    
    // full schedule
    if (get_page_template_slug() == 'schedule.php' ) {
        wp_enqueue_script('tyler-schedule', get_template_directory_uri() . '/js/schedule.js', array('jquery'));
    }
}

add_action('admin_enqueue_scripts', 'tyler_admin_enqueue_scripts');

add_action( 'wp_head', 'tyler_twitter_template_hash' );

function tyler_twitter_template_hash() {
	if (get_page_template_slug() == 'twitter.php') { ?>
	<script type="text/javascript">
		var tyler_twitter_hash ='<?php echo get_option('ef_twitter_widget_twitterhash'); ?>';
	</script>
	<?php
	}
}

function tyler_admin_enqueue_scripts($hook) {
    global $post_type;

    if (in_array($hook, array('post.php', 'post-new.php'))) {
        if ($post_type == 'session') {
            wp_enqueue_script('jquery-ui-datepicker');
            wp_enqueue_style('jquery-ui-datepicker', get_template_directory_uri() . '/css/admin/smoothness/jquery-ui-1.10.3.custom.min.css');
        } else if (get_page_template_slug() == 'speakers.php') {
            wp_enqueue_script('jquery-ui-sortable');
            wp_enqueue_script('tyler-page-speakers-full-screen', get_template_directory_uri() . '/js/page-speakers-full-screen.js');
            wp_enqueue_style('tyler-sortable', get_template_directory_uri() . '/css/sortable.css');
            wp_enqueue_style('jquery-ui-datepicker', get_template_directory_uri() . '/css/admin/smoothness/jquery-ui-1.10.3.custom.min.css');
        }
    }
}

// ******************* Ajax ****************** //

add_action('wp_ajax_nopriv_get_tweets', 'tyler_ajax_get_tweets');
add_action('wp_ajax_get_tweets', 'tyler_ajax_get_tweets');

function tyler_ajax_get_tweets() {
    global $twitter;
    $twitterhash = get_option('ef_twitter_widget_twitterhash');
    $tweets = array();

    if (isset($twitter) && !empty($twitterhash)) {
        $url = 'https://api.twitter.com/1.1/search/tweets.json';
        $getfield = "?q={$_GET['queryParams']['q']}&count={$_GET['queryParams']['count']}";
        $requestMethod = 'GET';
        $store = $twitter->setGetfield($getfield)
                    ->buildOauth($url, $requestMethod)
                    ->performRequest();
        $tweets = json_decode($store);
    }

    echo json_encode($tweets->statuses);
    die;
}

add_action('wp_ajax_nopriv_get_schedule', array( 'EF_Session_Helper', 'fudge_ajax_get_schedule' ) );
add_action('wp_ajax_get_schedule', array( 'EF_Session_Helper', 'fudge_ajax_get_schedule' ) );


// ******************* Misc ****************** //

add_filter('manage_edit-speaker_columns', 'edit_speaker_columns');

function edit_speaker_columns($columns) {
    $new_columns = array(
            'cb' => $columns['cb'],
            'title' => $columns['title'],
            'menu_order' => __('Order', 'tyler'),
            'date' => $columns['date'],
    );
    return $new_columns;
}

add_action('manage_posts_custom_column', 'edit_post_columns', 10, 2);

function edit_post_columns($column_name) {
    global $post;

    switch ($column_name) {
        case 'menu_order' :
            echo $post->menu_order;
            break;

        default:
    }
}

function getRelativeTime($date) {
    $diff = time() - strtotime($date);
    if ($diff < 60)
        return $diff . _n(' second', ' seconds', $diff, 'tyler') . __(' ago', 'tyler');
    $diff = round($diff / 60);
    if ($diff < 60)
        return $diff . _n(' minute', ' minutes', $diff, 'tyler') . __(' ago', 'tyler');
    $diff = round($diff / 60);
    if ($diff < 24)
        return $diff . _n(' hour', ' hours', $diff, 'tyler') . __(' ago', 'tyler');
    $diff = round($diff / 24);
    if ($diff < 7)
        return $diff . _n(' day', ' days', $diff, 'tyler') . __(' ago', 'tyler');
    $diff = round($diff / 7);
    if ($diff < 4)
        return $diff . _n(' week', ' weeks', $diff, 'tyler') . __(' ago', 'tyler');
    return __('on ', 'tyler') . date("F j, Y", strtotime($date));
}

add_filter('wp_nav_menu_items', 'tyler_wp_nav_menu_items', 10, 2);

function tyler_wp_nav_menu_items($items, $args) {
    if ($args->theme_location == 'primary' && is_active_widget(false, false, 'tyler_registration') && get_option('tyler_registration_widget_showtopmenu') == 1)
        $items .= '<li class="menu-item register"><a href="#tile_calltoaction">' . __('Register', 'tyler') . '</a></li>';
    return $items;
}

function tyler_get_video_gallery_attribute($video_type, $video_code) {
    $ret = '';

    switch ($video_type) {
        case 'youtube':
            $ret = "type='text/html' href='https://www.youtube.com/watch?v=$video_code' data-youtube='$video_code'";
            break;
        case 'vimeo':
            $ret = "type='text/html' href='https://vimeo.com/$video_code' data-vimeo='$video_code'";
            break;
    }

    return $ret;
}
