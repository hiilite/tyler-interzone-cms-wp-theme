<?php

add_action( 'add_meta_boxes', 'ef_session_metabox' );

function ef_session_metabox() { 
	add_meta_box('metabox-session', __('Session Details', 'dxef'), 
		'ef_metabox_session', 'session', 'normal', 'high');
	
	add_meta_box('metabox-session-speakers', __('Speakers', 'dxef'), 
		'ef_metabox_session_speakers', 'session', 'normal', 'high');
}

function ef_metabox_session($post) {
	$session_date = get_post_meta($post->ID, 'session_date', true);
	$session_time = get_post_meta($post->ID, 'session_time', true);
	$session_end_time = get_post_meta($post->ID, 'session_end_time', true);
	$session_home = get_post_meta($post->ID, 'session_home', true);
	$session_registration_code = get_post_meta($post->ID, 'session_registration_code', true);
	$session_registration_text = get_post_meta($post->ID, 'session_registration_text', true);
	?>
    <script type="text/javascript">
        jQuery(document).ready(function(){
            jQuery('#session_date_str').datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'mm/dd/yy',
                altFormat: 'yy-mm-dd',
                altField: '#session_date'
            });
        });
    </script>
    <p>
        <label for="session_home"><?php _e('Show in home page', 'dxef'); ?></label>
        <input type="checkbox" id="session_home" name="session_home" value="1" <?php if ($session_home == 1) echo 'checked="checked"'; ?> />
    </p>   
   	<p>
        <label for="session_date"><?php _e('Date', 'dxef'); ?></label>
        <input type="text" id="session_date_str" name="session_date" value="<?php echo!empty($session_date) ? date('m/d/Y', $session_date) : ''; ?>" />
    </p>   
    <p>
        <label for="session_time"><?php _e('Start Time', 'dxef'); ?></label>
        <input type="text" id="session_time" name="session_time" value="<?php echo $session_time; ?>" />
        <span><?php _e('Format hh:mm', 'dxef'); ?></span>
    </p>
    <p>
        <label for="session_end_time"><?php _e('End Time', 'dxef'); ?></label>
        <input type="text" id="session_end_time" name="session_end_time" value="<?php echo $session_end_time; ?>" />
        <span><?php _e('Format hh:mm', 'dxef'); ?></span>
    </p>
    <p>
        <label for="session_registration_code"><?php _e('Registration Embed Code:', 'dxef'); ?></label><br/>
        <textarea id="session_registration_code" name="session_registration_code" cols="50" rows="5"><?php echo $session_registration_code; ?></textarea>
    </p>
    <p>
        <label for="session_registration_text"><?php _e('Registration Text:', 'dxef'); ?></label><br/>
        <textarea id="session_registration_text" name="session_registration_text" cols="50" rows="5"><?php echo $session_registration_text; ?></textarea>
    </p>
    <?php
}

function ef_metabox_session_speakers($post) {
	$speakers = get_posts(array('post_type' => 'speaker', 'post_status' => 'publish', 'suppress_filters' => false, 'posts_per_page' => -1, 'orderby' => 'title', 'order' => 'ASC'));
	$session_speakers_list = get_post_meta($post->ID, 'session_speakers_list', true);
	?>
    <ul>
        <?php
        foreach ($speakers as $speaker) {
            $checked = '';
            if (!empty($session_speakers_list) && count($session_speakers_list) > 0 && in_array($speaker->ID, $session_speakers_list))
                $checked = 'checked="checked"';
            ?>
            <li>
                <input type="checkbox" name="session_speakers_list[]" id="session_speakers_list_<?php echo $speaker->ID; ?>" value="<?php echo $speaker->ID; ?>" <?php echo $checked; ?> />
                <label for="session_speakers_list_<?php echo $speaker->ID; ?>"><?php echo $speaker->post_title; ?></label>
            </li>
            <?php
        }
        ?>
    </ul>
    <?php
}

add_action( 'save_post', 'ef_session_save_post' );

function ef_session_save_post( $id ) {
	if( isset( $_POST['post_type'] ) && $_POST['post_type'] === 'session' ) {
		if (isset($_POST['session_home']))
			update_post_meta($id, 'session_home', $_POST['session_home']);
		else
			delete_post_meta($id, 'session_home');
				
		if (isset($_POST['session_date']))
			update_post_meta($id, 'session_date', strtotime($_POST['session_date']));
		
		if (isset($_POST['session_time']))
			update_post_meta($id, 'session_time', $_POST['session_time']);
		
		if (isset($_POST['session_end_time']))
			update_post_meta($id, 'session_end_time', $_POST['session_end_time']);
		
		if (isset($_POST['session_speakers_list']))
			update_post_meta($id, 'session_speakers_list', $_POST['session_speakers_list']);
		else
			delete_post_meta($id, 'session_speakers_list');
		
		if (isset($_POST['session_registration_code']))
			update_post_meta($id, 'session_registration_code', $_POST['session_registration_code']);
		else
			delete_post_meta($id, 'session_registration_code');
		
		if (isset($_POST['session_registration_text']))
			update_post_meta($id, 'session_registration_text', $_POST['session_registration_text']);
		else
			delete_post_meta($id, 'session_registration_text');

	}
}